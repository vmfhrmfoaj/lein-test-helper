(ns leiningen.test-plus
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [leiningen.core.eval :refer [eval-in-project]]
            [leiningen.core.project :refer [merge-profiles]]))


(def profiles
  "a dependencies of the test-plus."
  [:leiningen/test :test
   {:dependencies [['org.clojure/tools.namespace "1.3.0"]]}])


(defn test-plus
  [project & opts]
  (let [auto? (some #(= ":auto" %) opts)
        deps (->> project
                  :dependencies
                  (map #(vec (take 2 %)))
                  (into {}))
        ver->num (fn [major minor patch]
                   (+ (* major 10,000)
                      (* minor    100)
                      patch))
        clojure-ver (map #(Integer/parseInt %) (str/split (get deps 'org.clojure/clojure "") #"\."))
        included-spec? (< (ver->num 1 9 0) (apply ver->num clojure-ver))
        explain-str (if-not included-spec?
                      'identity
                      'clojure.spec.alpha/explain-str)]
    (eval-in-project
     (merge-profiles project profiles)

     `(do
        (alias '~'data     '~'clojure.data)
        (alias '~'io       '~'clojure.java.io)
        (alias '~'ns-dir   '~'clojure.tools.namespace.dir)
        (alias '~'ns-file  '~'clojure.tools.namespace.file)
        (alias '~'ns-repl  '~'clojure.tools.namespace.repl)
        (alias '~'ns-tool  '~'clojure.tools.namespace)
        (alias '~'ns-track '~'clojure.tools.namespace.track)
        (alias '~'pp       '~'clojure.pprint)
        (alias '~'repl     '~'clojure.repl)
        (alias '~'shell    '~'clojure.java.shell)
        (alias '~'str      '~'clojure.string)
        (alias '~'test     '~'clojure.test)
        (.addMethod clojure.core/print-method :custom (fn [o# ^java.io.Writer w#] (.write w# (.toString o#))))
        (let [changes-only# (get-in '~project [:test-refresh :changes-only])
              notify-trigger-type# (get-in '~project [:test-refresh :notify-trigger-type] :level)
              notify-cmd# (get-in '~project [:test-refresh :notify-command])
              notify# (comp (partial apply shell/sh) (partial conj notify-cmd#))
              timeout# (get-in '~project [:test-refresh :timeout-in-sec])
              dirs# (->> (select-keys '~project [:source-paths :test-paths])
                         (vals)
                         (apply concat)
                         (map io/as-file)
                         (filter #(.exists %))
                         (filter #(.isDirectory %)))
              cur-test-ns# (atom nil)
              retry-ns# (atom nil)
              set-cur-ns# #(reset! cur-test-ns# (:ns %))
              add-retry-ns# #(do (swap! retry-ns# conj (ns-name @cur-test-ns#)) %1)
              report-ns#    (get-method test/report :begin-test-ns)
              report-fail#  (get-method test/report :fail)
              report-error# (get-method test/report :error)
              style=>ansi-code# {:underline  "[4m"
                                 :fg-black   "[30m"
                                 :fg-red     "[31m"
                                 :fg-green   "[32m"
                                 :fg-yellow  "[33m"
                                 :fg-blue    "[34m"
                                 :fg-magenta "[35m"
                                 :fg-cyan    "[36m"
                                 :fg-white   "[37m"}
              styled-str# #(str \u001b (style=>ansi-code# %1) %2 \u001b "[0m")
              l-println# (fn [label# & more#]
                           (println (styled-str# :underline label#)
                                    (->> (apply print-str more#)
                                         (str/split-lines)
                                         (interpose (apply str "\n"
                                                           (-> label#
                                                               (count)
                                                               (inc)
                                                               (repeat " "))))
                                         (apply str))))
              pp-str# #(with-out-str (pp/pprint %))
              wrapping# #(with-meta (reify Object (toString [_] %)) {:type :custom})
              need-wrapping?# (some-fn number? keyword?)
              colorized# (fn colorized# [color# it# diff#]
                           (cond
                             (nil? diff#)            it#
                             (string? diff#)         (wrapping# (styled-str# color# (str "\"" it# "\"")))
                             (need-wrapping?# diff#) (wrapping# (styled-str# color# it#))
                             (vector? diff#)         (vec (colorized# color# it# (seq diff#)))

                             (sequential? diff#)
                             (map #(if (nil? %2)
                                     %1
                                     (colorized# color# %1 %2))
                                  it# (concat diff# (repeat nil)))

                             (map? diff#)
                             (->> it#
                                  (map (fn [[key# val#]]
                                         (let [diff# (get diff# key#)
                                               key# (if (nil? diff#)
                                                      key#
                                                      (colorized# color# key# key#))
                                               val# (if (nil? diff#)
                                                      val#
                                                      (colorized# color# val# diff#))]
                                           [key# val#])))
                                  (into {}))

                             (set? diff#)
                             (->> it#
                                  (map #(cond->> % (diff# %) (colorized# color# %)))
                                  (into #{}))

                             :else (wrapping# (styled-str# color# (str it#)))))

              report-fail+#
              (fn [m#]
                (test/with-test-out
                  (when-let [[_# [pred# a# b#]] (:actual m#)]
                    (cond
                      (= "=" (name pred#))
                      (let [[a'# b'#] (data/diff a# b#)]
                        (l-println# "expected:" (pp-str# (colorized# :fg-red  a# a'#)))
                        (l-println# "  result:" (pp-str# (colorized# :fg-cyan b# b'#))))

                      (and (find-ns 'clojure.spec.alpha) (= "valid?" (name pred#)))
                      (l-println# "explanation:" (~explain-str a# b#))

                      :else nil))))

              last-result# (atom true)]
          (.addMethod test/report :begin-test-ns #(do (set-cur-ns# %) (report-ns# %)))
          (.addMethod test/report :fail  #(do (add-retry-ns# %) (report-fail# %) (report-fail+# %)))
          (.addMethod test/report :error #(do (add-retry-ns# %) (report-error# %)))
          (apply ns-repl/set-refresh-dirs dirs#)

          ;; main loop =>
          (loop [tracker# (apply ns-dir/scan (ns-track/tracker) dirs#)]
            (if-not (= :ok (ns-repl/refresh))
              (repl/pst *e)
              (let [ret# (promise)
                    thread# (doto (Thread.
                                   (fn []
                                     (let [ns# @retry-ns#]
                                       (reset! retry-ns# nil)
                                       (deliver ret#
                                                ;; TODO
                                                ;;  catch run-time errors
                                                (apply test/run-tests
                                                       (if changes-only#
                                                         (->> tracker#
                                                              (:clojure.tools.namespace.track/load)
                                                              (filter #(str/ends-with? (name %) "-test"))
                                                              (concat ns#)
                                                              (distinct))
                                                         (filter #(str/ends-with? (-> % (ns-name) (name)) "-test") (all-ns))))))))
                              (.start))
                    result# (if-not (and (integer? timeout#)
                                         (pos?     timeout#))
                              @ret#
                              (deref ret# (* 1000 timeout#) :timeout))]
                (when (= :timeout result#)
                  (println "Test timed out!!")
                  (.stop thread#))
                (when notify-cmd#
                  (if (= :timeout result#)
                    (notify# "Test timed out!")
                    (let [num-not-passed-tests# (->> [:fail :error]
                                                     (select-keys result#)
                                                     (vals)
                                                     (apply +))
                          num-tests# (+ (:pass result#) num-not-passed-tests#)
                          [last-res# cur-res#] (reset-vals! last-result# (zero? num-not-passed-tests#))]
                      (when (or (and (= notify-trigger-type# :level) (not cur-res#))
                                (and (= notify-trigger-type# :edge)  (not= last-res# cur-res#)))
                        (notify# (str num-tests# " tests, "
                                      (:fail  result#) " failures, "
                                      (:error result#) " errors."))))))))

            (when ~auto?
              (println "test done:"
                       (-> (System/currentTimeMillis)
                           (java.sql.Timestamp.)
                           (str)))
              (println (apply str (repeat 78 "-")))
              ;; will be released when src and test files are changed.
              (recur (let [refresh-time# (:clojure.tools.namespace.dir/time ns-repl/refresh-tracker)]
                       (loop [new-tracker# ns-repl/refresh-tracker]
                         (if-not (= refresh-time# (:clojure.tools.namespace.dir/time new-tracker#))
                           new-tracker#
                           (do
                             (Thread/sleep 500)
                             (recur (apply ns-dir/scan new-tracker# dirs#)))))))))
          ;; NOTE
          ;; the `clojure.java.shell/sh` use the future and
          ;;  it's waiting to terminate a thread-pool of the agent.
          ;; see also:
          ;; - http://dev.clojure.org/jira/browse/CLJ-959
          ;; - http://dev.clojure.org/jira/browse/CLJ-124
          (shutdown-agents)))

     `(do
        (require 'clojure.data)
        (require 'clojure.java.io)
        (require 'clojure.java.shell)
        (require 'clojure.pprint)
        (require 'clojure.repl)
        (require 'clojure.string)
        (require 'clojure.test)
        (require 'clojure.tools.namespace)
        (require 'clojure.tools.namespace.dir)
        (require 'clojure.tools.namespace.file)
        (require 'clojure.tools.namespace.repl)
        (require 'clojure.tools.namespace.track)
        (if ~included-spec?
          (require 'clojure.spec.alpha))))))
